from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from blog.models import Entry
from django.utils import timezone




def adminBlog(request):
	entries = Entry.objects.all()
	return render(request, 'blog/adminBlog.html', {
		'entries': entries })



#@staff_member_required
def edit(request, titleUrl):
	e = get_object_or_404(Entry, titleUrl=titleUrl)
	return render(request, 'blog/entry.html', {
		'Entry': e })

#@staff_member_required
def save(request, titleUrl):
	p = request.POST
	title = request.POST['title']
	text = request.POST['text']

	if title != "":
		Entry.objects.filter(titleUrl=titleUrl).update(title=title, titleUrl=title.replace(" ", "-").lower(),pub_date = timezone.now())

	if text != "":
		Entry.objects.filter(titleUrl=titleUrl).update(text=text,pub_date = timezone.now())

	return HttpResponseRedirect(reverse('adminBlog'))

#@staff_member_required
def new(request):
	return render(request, 'blog/entry.html')

#@staff_member_required
def savenew(request):
	p = request.POST
	title = request.POST['title']
	text = request.POST['text']

	if title != "" and text != "":
		e = Entry(title=title,text=text,titleUrl=title.replace(" ", "-").lower(),pub_date = timezone.now())
		e.save()

	return HttpResponseRedirect(reverse('adminBlog'))

#@staff_member_required
def delete(request, titleUrl):
	e = get_object_or_404(Entry, titleUrl=titleUrl)
	e.delete()
	return HttpResponseRedirect(reverse('adminBlog'))


def blog(request):
	entries = Entry.objects.all()
	return render(request, 'blog/blog.html', {
		'entries': entries })

