from django.db import models
from django.utils import timezone


class Entry(models.Model):
	title = models.CharField(max_length=200)
	titleUrl = models.CharField(max_length=200, blank=True)
	text = models.CharField(max_length=10000)
	pub_date = models.DateTimeField()

	def __unicode__(self):
		return self.title

	def save(self, *args, **kwargs):
		self.titleUrl = self.title.replace(" ", "-").lower()
		self.pub_date = timezone.now()
		super(Entry,self).save(*args,**kwargs)
