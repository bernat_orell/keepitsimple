from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^adminBlog/', 'blog.views.adminBlog', name='adminBlog'),
    url(r'^edit/(?P<titleUrl>[a-z\-]+)', 'blog.views.edit', name='edit'),
    url(r'^delete/(?P<titleUrl>[a-z\-]+)', 'blog.views.delete', name='delete'),
    url(r'^save/(?P<titleUrl>[a-z\-]+)', 'blog.views.save', name='save'),
    url(r'^new/', 'blog.views.new', name='new'),
    url(r'^savenew/', 'blog.views.savenew', name='savenew'),
    url(r'^$', 'blog.views.blog', name="blog"),
)